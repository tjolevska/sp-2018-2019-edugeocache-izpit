# Navodila za vzpostavitev delovnega okolja za pisni izpit


## Fork izvornega repozitorija

1. Aplikacija **EduGeoCache**, ki smo jo razvijali med predavanji, je na voljo v Bitbucket repozitoriju **[SP 2018-2019 - EduGeoCache - Izpit](https://bitbucket.org/dlavbic/sp-2018-2019-edugeocache-izpit)**.

2. Izvedite fork omenjenega repozitorija, tako da obiščete spletni naslov [https://bitbucket.org/dlavbic/sp-2018-2019-edugeocache-izpit/**fork**](https://bitbucket.org/dlavbic/sp-2018-2019-edugeocache-izpit/fork).

3. Pri kreiranju lastne kopije repozitorija je potrebno obvezno določiti:

	* **ime repozitorija** nastavite na **`sp-izpit-x`**, kjer **`x`** predstavlja zaporedno številko izpita (npr. `sp-izpit-1`, `sp-izpit-2` oz. `sp-izpit-3`),
	* repozitorij mora biti **privaten**.


## Dostop do privatnega repozitorija

1. Uporabnikoma **`AssistentFRI`** in **`dlavbic`** dodajte dostop do obstoječe lastne privatne kopije repozitorija.


## Priprava Cloud9 razvojnega okolja

1. V obstoječem Cloud9 razvojnem okolju (ki ste ga uporabljali pri vajah in za izdelavo lastnega projekta) se prepričajte, da se nahajate v svoji uporabniški mapi `~/workspace`.

2. Kreirajte lokalno kopijo vašega privatnega repozitorija iz prejšnjega koraka, kjer je:
	* `<uporabnisko-ime>` vaše Bitbucket uporabniško ime (npr. `dlavbic`) in
	* `<x>` zaporedna številka izpita (npr. `1`, `2` ali `3`).

	```
	~/workspace $ git clone git@bitbucket.org:<uporabnisko-ime>/sp-izpit-<x>.git
	```

3. Prestavite se v ravnokar kreirano mapo `sp-izpit-<x>`.

	```
	~/workspace $ cd sp-izpit-<x>
	```

3. Namestite vse potrebne knjižnice.

	```	
	~/workspace/sp-izpit-<x> (master) $ npm install
	```

3. Poskrbite za okoljske spremenljivke.

	```
	~/workspace/sp-izpit-<x> (master) $ echo "JWT_GESLO=toleNašeGeslo" > .env
	```

## Zagon podatkovne baze

1. V ločenem zavihku Cloud9 okolja poženite MongoDB podatkovno bazo.


## Uvoz testnih podatkov

1. V predhodnjem zavihku terminala (kjer se nahajate v mapi `~/workspace/sp-izpit-<x>`) zahtevajte brisanje morebitne obstoječe podatkovne baze z imenom `edugeocache`.

	```
	~/workspace/sp-izpit-<x> (master) $ mongo edugeocache --eval "db.dropDatabase()"
	```

2. V podatkovno bazo uvozite testne podatke iz datoteke `testni_podatki.json`, ki se nahaja v mapi `app_api/models`.

	```
	~/workspace/sp-izpit-<x> (master) $ mongoimport --db edugeocache --collection Lokacije --mode upsert --upsertFields naziv --jsonArray --file ./app_api/models/testni_podatki.json
	```

3. V podatkovni bazi popravite ID-je odvisnih dokumentov (t.j. komentarjev).

	```
	~/workspace/sp-izpit-<x> (master) $ mongo edugeocache --eval "db.Lokacije.find().forEach(function(dokument) { for (var i = 0; i < dokument.komentarji.length; i++) { dokument.komentarji[i]._id = ObjectId() } db.Lokacije.update({ '_id' : dokument._id }, dokument) })"
	```

## Zagon aplikacije

1. Aplikacijo poženete z ukazom `nodemon`

	```
	~/workspace/sp-izpit-<x> (master) $ nodemon
	```

2. Preverite delovanje aplikacije in registrirajte testnega uporabnika v sistem, s katerim boste na izpitu izvajali testiranje.