var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.komentarjiKreiraj = function(zahteva, odgovor) {
  vrniAvtorja(zahteva, odgovor, function(zahteva, odgovor, imeUporabnika) {
    var idLokacije = zahteva.params.idLokacije;
    if (idLokacije) {
      Lokacija
        .findById(idLokacije)
        .select('komentarji')
        .exec(
          function(napaka, lokacija) {
            if (napaka) {
              vrniJsonOdgovor(odgovor, 400, napaka);
            } else {
              dodajKomentar(zahteva, odgovor, lokacija, imeUporabnika);
            }
          }
        );
    } else {
      vrniJsonOdgovor(odgovor, 400, {
        "sporočilo": 
          "Ne najdem lokacije, idLokacije je obvezen parameter."
      });
    }
  });
};

module.exports.komentarjiPreberiIzbranega = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije && zahteva.params.idKomentarja) {
    Lokacija
      .findById(zahteva.params.idLokacije)
      .select('naziv komentarji')
      .exec(
        function(napaka, lokacija) {
          var komentar;
          if (!lokacija) {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": 
                "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije."
            });
            return;
          } else if (napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
          }
          if (lokacija.komentarji && lokacija.komentarji.length > 0) {
            komentar = lokacija.komentarji.id(zahteva.params.idKomentarja);
            if (!komentar) {
              vrniJsonOdgovor(odgovor, 404, {
                "sporočilo": 
                  "Ne najdem komentarja s podanim enoličnim identifikatorjem idKomentarja."
              });
            } else {
              vrniJsonOdgovor(odgovor, 200, {
                lokacija: {
                  naziv: lokacija.naziv,
                  id: zahteva.params.idLokacije
                },
                komentar: komentar
              });
            }
          } else {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": "Ne najdem nobenega komentarja."
            });
          }
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem zapisa, oba enolična identifikatorja idLokacije in idKomentarja sta zahtevana."
    });
  }
};

module.exports.komentarjiPosodobiIzbranega = function(zahteva, odgovor) {
  if (!zahteva.params.idLokacije || !zahteva.params.idKomentarja) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije oz. komentarja, " + 
        "idLokacije in idKomentarja sta obvezna parametra."
    });
    return;
  }
  Lokacija
    .findById(zahteva.params.idLokacije)
    .select('komentarji')
    .exec(
      function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem lokacije."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        if (lokacija.komentarji && lokacija.komentarji.length > 0) {
          var trenutniKomentar = 
            lokacija.komentarji.id(zahteva.params.idKomentarja);
          if (!trenutniKomentar) {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": "Komentarja ne najdem."
            });
          } else {
            trenutniKomentar.avtor = zahteva.body.naziv;
            trenutniKomentar.ocena = zahteva.body.ocena;
            trenutniKomentar.besediloKomentarja = zahteva.body.komentar;
            lokacija.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 400, napaka);
              } else {
                posodobiPovprecnoOceno(lokacija._id);
                vrniJsonOdgovor(odgovor, 200, trenutniKomentar);
              }
            });
          }
        } else {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ni komentarjev za ažuriranje."
          });
        }
      }
    );
};

module.exports.komentarjiIzbrisiIzbranega = function(zahteva, odgovor) {
  if (!zahteva.params.idLokacije || !zahteva.params.idKomentarja) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije oz. komentarja, " + 
        "idLokacije in idKomentarja sta obvezna parametra."
    });
    return;
  }
  Lokacija
    .findById(zahteva.params.idLokacije)
    .exec(
      function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Lokacije ne najdem."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        if (lokacija.komentarji && lokacija.komentarji.length > 0) {
          if (!lokacija.komentarji.id(zahteva.params.idKomentarja)) {
            vrniJsonOdgovor(odgovor, 404, {
              "sporočilo": "Komentarja ne najdem."
            });
          } else {
            lokacija.komentarji.id(zahteva.params.idKomentarja).remove();
            lokacija.save(function(napaka) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
              } else {
                posodobiPovprecnoOceno(lokacija._id);
                vrniJsonOdgovor(odgovor, 204, null);
              }
            });
          }
        } else {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ni komentarja za brisanje."
          });
        }
      }
    );
};

var dodajKomentar = function(zahteva, odgovor, lokacija, avtor) {
  if (!lokacija) {
    vrniJsonOdgovor(odgovor, 404, {
      "sporočilo": "Ne najdem lokacije."
    });
  } else if (isNaN(parseInt(zahteva.body.ocena, 10))) {
    vrniJsonOdgovor(odgovor, 404, {
      "sporočilo": "Napačna zahteva!"
    });
  } else {
    lokacija.komentarji.push({
      avtor: avtor,
      ocena: zahteva.body.ocena,
      besediloKomentarja: zahteva.body.komentar
    });
    lokacija.save(function(napaka, lokacija) {
      var dodaniKomentar;
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        posodobiPovprecnoOceno(lokacija._id);
        dodaniKomentar = lokacija.komentarji[lokacija.komentarji.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniKomentar);
      }
    });
  }
};

var posodobiPovprecnoOceno = function(idLokacije) {
  Lokacija
    .findById(idLokacije)
    .select('ocena komentarji')
    .exec(
      function(napaka, lokacija) {
        if (!napaka)
          izracunajPovprecnoOceno(lokacija);
      }
    );
};

var izracunajPovprecnoOceno = function(lokacija) {
  if (lokacija.komentarji && lokacija.komentarji.length > 0) {
    var steviloKomentarjev = lokacija.komentarji.length;
    var skupnaOcena = 0;
    for (var stevec = 0; stevec < steviloKomentarjev; stevec++) {
      skupnaOcena += lokacija.komentarji[stevec].ocena;
    }
    var povprecnaOcena = parseInt(skupnaOcena / steviloKomentarjev, 10);
    lokacija.ocena = povprecnaOcena;
    lokacija.save(function(napaka) {
      if (napaka) {
        console.log(napaka);
      } else {
        console.log("Povprečna ocena je posodobljena na " + povprecnaOcena + ".");
      }
    });
  }
};

var vrniAvtorja = function(zahteva, odgovor, povratniKlic) {
  if (zahteva.payload && zahteva.payload.elektronskiNaslov) {
    Uporabnik
      .findOne({
        elektronskiNaslov: zahteva.payload.elektronskiNaslov
      })
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        povratniKlic(zahteva, odgovor, uporabnik.ime);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Ni podatka o uporabniku"
    });
    return;
  }
};