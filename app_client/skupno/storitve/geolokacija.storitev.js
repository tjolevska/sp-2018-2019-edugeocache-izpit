(function() {
  var geolokacija = function() {
    var vrniLokacijo = function(pkUspesno, pkNapaka, pkNiLokacije) {
      /* global navigator */
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(pkUspesno, pkNapaka);
      } else {
        pkNiLokacije();
      }
    };
    return {
      vrniLokacijo: vrniLokacijo
    };
  };
  
  /* global angular */
  angular
    .module('edugeocache')
    .service('geolokacija', geolokacija);
})();